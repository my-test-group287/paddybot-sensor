#include <Arduino.h>
#include <pgmspace.h>

#include <WiFiClientSecure.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>
#include "WiFi.h"
#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>

// // UUID
static const char *const DRYERA1 PROGMEM = "b5153d6f-8bda-4004-a7b0-9e403cf5f333";   // Dryer A1
//static const char *const DRYERA1 PROGMEM = "a8f6a629-6790-4cb4-a6e1-f04d13065d0f";   // Dryer A1 //test uuid
static const char *const DRYERA1_ PROGMEM = "107e369a-9f82-43c1-ad12-9b91bdee917a";  // Dryer A1.
static const char *const DRYERA2 PROGMEM = "eefd24e6-53ac-404c-a6c7-93ebf3ac34fe";   // Dryer A2
static const char *const DRYERA2_ PROGMEM = "571ebb72-a9d4-4de6-be6a-2d0deb7a6a61";  // Dryer A2.
static const char *const DRYERA3 PROGMEM = "2993dc00-1f84-48a6-b30d-1ce4f3f43445";   // Dryer A3
static const char *const DRYERA3_ PROGMEM = "c17ac968-106b-493a-b1e9-6796a7a4a0e8";  // Dryer A3.
static const char *const DRYERA4 PROGMEM = "116e9fe0-6e05-4e49-a18e-191834dbc56c";   // Dryer A4
static const char *const DRYERA4_ PROGMEM = "f750df7e-15a6-4244-868e-843454a52a61";  // Dryer A4.
static const char *const DRYERA5 PROGMEM = "b1d49ddf-6a7b-424d-b202-9b3715b5c74a";   // Dryer A5
static const char *const DRYERA5_ PROGMEM = "f827b10c-d463-4ae4-b2f1-ea890480b065";  // Dryer A5.
static const char *const DRYERA6 PROGMEM = "81ae2ff4-8f6b-4c26-b88b-24163c422917";   // Dryer A6
static const char *const DRYERA6_ PROGMEM = "01ab3267-b70c-49d3-a7aa-e7c22f044dee";  // Dryer A6.
static const char *const DRYERA7 PROGMEM = "0c141b4b-cf35-4f01-aa52-c92364529afd";   // Dryer A7
static const char *const DRYERA7_ PROGMEM = "035342a4-5414-4ae0-998b-99dd907b070b";  // Dryer A7.
static const char *const DRYERA8 PROGMEM = "4ca3edf9-c00f-4490-b2b0-c05a842609ca";   // Dryer A8
static const char *const DRYERA8_ PROGMEM = "c6b30a05-f792-4795-8b93-4c13ba50f17e";  // Dryer A8.
static const char *const DRYERA9 PROGMEM = "0ffae8d7-6242-4585-90f1-73b03d9345f4";   // Dryer A9
static const char *const DRYERA9_ PROGMEM = "471f067b-aff6-40ed-9629-32a19560c16e";  // Dryer A9.
static const char *const DRYERA10 PROGMEM = "fe6f153b-b6f5-4c18-b30e-0934150b2249";  // Dryer A10
static const char *const DRYERA10_ PROGMEM = "293b0f25-be3c-488b-b93b-8e5b4ba4b9d2"; // Dryer A10.
static const char *const DRYERA11 PROGMEM = "6bfaf4e8-50e6-4120-9cd9-d2639be9bded";  // Dryer A11
static const char *const DRYERA11_ PROGMEM = "2b74dc15-d8bf-467d-b15d-e25c3dff260b"; // Dryer A11.
static const char *const DRYERA12 PROGMEM = "79f1fa42-ed76-4330-a026-bebf9171de78";  // Dryer A12
static const char *const DRYERA12_ PROGMEM = "fc474bb1-15ab-4bbe-9f4c-cd785cbda1f6"; // Dryer A12.
static const char *const DRYERA13 PROGMEM = "517f1b72-0ea3-4a53-a915-ae8dca1568de";  // Dryer A13
static const char *const DRYERA13_ PROGMEM = "9d63bb9a-1f70-49ff-93d0-4c7e3a9df09f"; // Dryer A13.
static const char *const DRYERA14 PROGMEM = "fd2de1c6-3f89-4965-af03-fed5959ca716";  // Dryer A14
static const char *const DRYERA14_ PROGMEM = "e722e20e-1b44-4fd4-bd35-57c425d91ce7"; // Dryer A14.

// // // Dryer B
static const char *const DRYERB1 PROGMEM = "d8353618-0e97-4172-ba2e-e008304bd39f";   // Dryer B1
static const char *const DRYERB1_ PROGMEM = "d8353618-0e97-4172-ba2e-e008304bd39f";  // Dryer B1.
static const char *const DRYERB2 PROGMEM = "2ef7ff5f-ab58-4070-9f1f-76ad37a560aa";   // Dryer B2
static const char *const DRYERB2_ PROGMEM = "398d2cb6-3c6f-444c-8d4c-51607bf87398";  // Dryer B2.
static const char *const DRYERB3 PROGMEM = "ac183ee7-ce4c-40f5-9354-71875bfe20e4";   // Dryer B3
static const char *const DRYERB3_ PROGMEM = "61a85b12-d14b-4805-ae74-4a3524948693";  // Dryer B3.
static const char *const DRYERB4 PROGMEM = "e26521ec-c84f-4e36-a56f-13a12ccd89be";   // Dryer B4
static const char *const DRYERB4_ PROGMEM = "142babd2-b581-463f-ac34-411ce109a1ef";  // Dryer B4.

// // // Dryer C
static const char *const DRYERC1 PROGMEM = "ab8ca1bd-6dae-460b-8d41-1a29970f36ab";   // Dryer C1
static const char *const DRYERC1_ PROGMEM = "c1bb2269-045b-4f67-9155-8dae1b30c8ba";  // Dryer C1.
static const char *const DRYERC2 PROGMEM = "b1f116f1-078b-413d-aeec-ac4a1d0e9707";   // Dryer C2
static const char *const DRYERC2_ PROGMEM = "aa9d656f-6589-449f-8c29-d370d2d7eeaa";  // Dryer C2.
static const char *const DRYERC3 PROGMEM = "b57a5a23-c9a7-46be-9137-2b5d3217f124";   // Dryer C3
static const char *const DRYERC3_ PROGMEM = "b57a5a23-c9a7-46be-9137-2b5d3217f124";  // Dryer C3.
static const char *const DRYERC4 PROGMEM = "c479b4ef-0758-4536-a583-dff87a423250";   // Dryer C4
static const char *const DRYERC4_ PROGMEM = "f32cc668-be8a-41e4-9036-6a7042eb00b9";  // Dryer C4.
static const char *const DRYERC5 PROGMEM = "053608e8-a0b3-4652-b9d2-d617fa9bec0b";   // Dryer C5
static const char *const DRYERC5_ PROGMEM = "c00e868d-9cbe-4976-9ea4-2a992d3669b1";  // Dryer C5.
static const char *const DRYERC6 PROGMEM = "19e9e2ea-d7bf-49d1-97b3-134ae2b7f491";   // Dryer C6
static const char *const DRYERC6_ PROGMEM = "0ac0b805-e96b-4d50-a7a5-71fe81933d58";  // Dryer C6.
static const char *const DRYERC7 PROGMEM = "0e7c1f87-31ef-44e1-b810-a9d9ca048a36";   // Dryer C7
static const char *const DRYERC7_ PROGMEM = "53b935bd-dde4-4bc9-b149-9bc7f6dc224f";  // Dryer C7.
static const char *const DRYERC8 PROGMEM = "58066a8f-37e0-4696-a919-179ab732ddb6";   // Dryer C8
static const char *const DRYERC8_ PROGMEM = "1478c400-b93b-47fd-ab23-ea32442438e3";  // Dryer C8.
static const char *const DRYERC9 PROGMEM = "f0d4d851-9cfc-4eef-bfe6-af81f8aa4975";   // Dryer C9
static const char *const DRYERC9_ PROGMEM = "0a798fc3-44cf-4d5f-ac48-7b95473cfbbf";  // Dryer C9.
static const char *const DRYERC10 PROGMEM = "70766ce5-c010-45db-8e6c-024beb277419";   // Dryer C10
static const char *const DRYERC10_ PROGMEM = "0777f2a9-72b9-422e-b3c9-8707a50bfe3c";  // Dryer C10.

// // // Dryer D
static const char *const DRYERD1 PROGMEM = "429cfc23-4f74-45d6-b0de-e86409e0fab2";   // Dryer D1
static const char *const DRYERD1_ PROGMEM = "406161af-0d82-4756-8ef8-e9a8f5c9fbbf";  // Dryer D1.
static const char *const DRYERD2 PROGMEM = "6f9cd8a1-b366-457c-9889-ff88b2870be4";   // Dryer D2
static const char *const DRYERD2_ PROGMEM = "e734c989-cf47-4ab6-902d-d79e4ef6e0e1";  // Dryer D2.
static const char *const DRYERD3 PROGMEM = "5355f4c0-f385-45dd-86ea-42b47945dedd";   // Dryer D3
static const char *const DRYERD3_ PROGMEM = "0ea06926-0cd3-418a-9dfd-5b61fc1e371a";  // Dryer D3.
static const char *const DRYERD4 PROGMEM = "f56d5911-562f-48df-bb17-e79f4cb21a0a";   // Dryer D4
static const char *const DRYERD4_ PROGMEM = "782a3b39-13ac-4950-88e4-c43581b398a0";  // Dryer D4.
static const char *const DRYERD5 PROGMEM = "9e2f5f44-3b60-4680-8533-0572a357b026";   // Dryer D5
static const char *const DRYERD5_ PROGMEM = "697a20fc-1f22-46a5-903d-dc0543faf3f3";  // Dryer D5.
static const char *const DRYERD6 PROGMEM = "2b325079-7980-4331-bf63-04417c63d34f";   // Dryer D6
static const char *const DRYERD6_ PROGMEM = "7ee106ce-b426-40d5-ae2f-89a35e8d1df7";  // Dryer D6.
static const char *const DRYERD7 PROGMEM = "31d2440a-2447-40a5-836c-bd631c5b3a31";   // Dryer D7
static const char *const DRYERD7_ PROGMEM = "14e178a5-43fb-4db2-87b3-711fd1950fdc";  // Dryer D7.
static const char *const DRYERD8 PROGMEM = "9c63793c-809a-4f5e-b643-a95386112690";   // Dryer D8
static const char *const DRYERD8_ PROGMEM = "4118c530-246d-4384-b814-ad34cd90306b";  // Dryer D8.
static const char *const DRYERD9 PROGMEM = "cf26f40c-c837-45d0-b6ca-dcdb06a0b86d";   // Dryer D9
static const char *const DRYERD9_ PROGMEM = "6cbec982-3940-47de-9e25-80bc54375add";  // Dryer D9.
static const char *const DRYERD10 PROGMEM = "3932e0da-1441-4224-b656-5702363338e0";   // Dryer D10
static const char *const DRYERD10_ PROGMEM = "a9090793-0601-4944-b954-827ef9107f53";  // Dryer D10.
static const char *const DRYERD11 PROGMEM = "d12972b7-f499-4718-9048-5188bf917bf2";   // Dryer D11
static const char *const DRYERD11_ PROGMEM = "a67175b8-aaa0-4992-b0b3-d5eed5b55518";  // Dryer D11.
static const char *const DRYERD12 PROGMEM = "412ac649-969b-450b-9fc4-45fe877875d6";   // Dryer D12
static const char *const DRYERD12_ PROGMEM = "2d1759f4-4e09-4236-af5a-d8276acba1da";  // Dryer D12.

// // // Dryer F
static const char *const DRYERF1 PROGMEM = "676934ea-11e1-4bda-9fe5-b6a52d9ac07c";   // Dryer F1
static const char *const DRYERF1_ PROGMEM = "b356240d-395e-4a0a-a76d-a202afaa554f";  // Dryer F1.
static const char *const DRYERF2 PROGMEM = "17aec4f1-bc3d-4157-bf92-bd91234f1e98";   // Dryer F2
static const char *const DRYERF2_ PROGMEM = "3dd49225-6f0a-4201-b7f1-ffdd197379c7";  // Dryer F2.
static const char *const DRYERF3 PROGMEM = "d377efe5-831a-4e05-aa6d-5bac57c8b93a";   // Dryer F3
static const char *const DRYERF3_ PROGMEM = "f7de068f-ecc3-4713-a4e9-0c85ec86c70e";  // Dryer F3.
static const char *const DRYERF4 PROGMEM = "d0f3b6b9-91e9-49fa-94c4-56eccab93614";  // Dryer F4
static const char *const DRYERF4_ PROGMEM = "f24d472a-7f81-42d8-aef0-5bd430a401ce";   // Dryer F4.
static const char *const DRYERF5 PROGMEM = "3158b31f-8df4-479d-992c-c59645a4587a";  // Dryer F5
static const char *const DRYERF5_ PROGMEM = "d14f0045-394f-4131-a835-fbfd68d82718";   // Dryer F5.
static const char *const DRYERF6 PROGMEM = "c602a4be-93ab-413b-9f37-950184295743";  // Dryer F6
static const char *const DRYERF6_ PROGMEM = "74219686-6238-41d8-94bc-1bcd932d6244";  // Dryer F6.
static const char *const DRYERF7 PROGMEM = "faf03e61-ba0e-456f-aaa5-51a054553f21";   // Dryer F7
static const char *const DRYERF7_ PROGMEM = "10e73f26-bc85-4b89-9666-6a17a0d2801c";  // Dryer F7.
static const char *const DRYERF8 PROGMEM = "e37e4af2-a97d-4a61-af11-c635dc928b7d";   // Dryer F8
static const char *const DRYERF8_ PROGMEM = "4dac5b63-7e87-45f2-87ad-c6a1b4725c36";  // Dryer F8.
static const char *const DRYERF9 PROGMEM = "4714325d-4422-4449-822b-5a433824e226";   // Dryer F9
static const char *const DRYERF9_ PROGMEM = "0f086c5e-78f3-4c22-a2ce-016c994ee8ac";  // Dryer F9.
static const char *const DRYERF10 PROGMEM = "9064d958-d912-448f-9f9e-028b82bf8b1a";  // Dryer F10
static const char *const DRYERF10_ PROGMEM = "6a60aa77-9efb-47f1-be7e-9099a385b85e";   // Dryer F10.
static const char *const DRYERF11 PROGMEM = "57c4d4a7-b4ba-4323-bc19-22d21b069b21";  // Dryer F11
static const char *const DRYERF11_ PROGMEM = "44723687-e1bc-42d3-a3c7-0a5f6dfe62b0";   // Dryer F11.
static const char *const DRYERF12 PROGMEM = "62ca1be4-6312-4fac-a609-53c68a7e8345";  // Dryer F12
static const char *const DRYERF12_ PROGMEM = "bc800604-23c7-47a2-a2ff-c71be3dfa13c";  // Dryer F12.

// // // Dryer G
static const char *const DRYERG1 PROGMEM = "d983d829-a9a7-405e-904a-31a72c20d03d";   // Dryer G1
static const char *const DRYERG1_ PROGMEM = "d5a4bb4d-6bba-45cd-aed8-2053a989de72";  // Dryer G1.
static const char *const DRYERG2 PROGMEM = "34667731-52eb-4d83-86b2-7d530d826262";   // Dryer G2
static const char *const DRYERG2_ PROGMEM = "4583c091-e91a-44cc-8966-e68d015469ed";  // Dryer G2.
static const char *const DRYERG3 PROGMEM = "f0d64d37-ec7a-4d47-a607-7f42e412a529";   // Dryer G3
static const char *const DRYERG3_ PROGMEM = "ed6a815b-4604-49a1-ac79-4a56450a9885";  // Dryer G3.
static const char *const DRYERG4 PROGMEM = "ec36a183-7694-4992-974b-2e18647a593b";  // Dryer G4
static const char *const DRYERG4_ PROGMEM = "4a0eebd0-ebb8-4a12-a20f-72a3e8eb9ead";   // Dryer G4.
static const char *const DRYERG5 PROGMEM = "fdc3e12c-bcf1-415b-b199-06f235505476";  // Dryer G5
static const char *const DRYERG5_ PROGMEM = "1256aaef-1021-4dd5-89a6-af466e763508";   // Dryer G5.
static const char *const DRYERG6 PROGMEM = "9a624d42-041e-4dcd-a2e1-01ff849f41fa";  // Dryer G6
static const char *const DRYERG6_ PROGMEM = "848a7cb5-4054-45fd-a8c7-85484ea8502e";  // Dryer G6.
static const char *const DRYERG7 PROGMEM = "bc6a53cf-a705-417c-b39a-4270c79107f9";   // Dryer G7
static const char *const DRYERG7_ PROGMEM = "70691362-d499-4c5c-8b92-c126440adfee";  // Dryer G7.
static const char *const DRYERG8 PROGMEM = "e8483576-db97-46c8-ad94-e6401b78320d";   // Dryer G8
static const char *const DRYERG8_ PROGMEM = "8628514f-b61c-4b52-9979-d4a214c25aa6";  // Dryer G8.

// // // DRYER A1 UUID 1 - 17
// static const char *const DRYERA1ID1 PROGMEM = "4a00c883-a252-4760-86c6-03b62048e667"; // Single Device A1 S1
// static const char *const DRYERA1ID2 PROGMEM = "cc6ebb2f-4693-4bf7-b5f3-59c422656a15"; // Single Device A1 S2
// static const char *const DRYERA1ID3 PROGMEM = "";                                     // Single Device A1 S3
// static const char *const DRYERA1ID4 PROGMEM = "";                                     // Single Device A1 S4
// static const char *const DRYERA1ID5 PROGMEM = "";                                     // Single Device A1 S5
// static const char *const DRYERA1ID6 PROGMEM = "";                                     // Single Device A1 S6
// static const char *const DRYERA1ID7 PROGMEM = "";                                     // Single Device A1 S7
// static const char *const DRYERA1ID8 PROGMEM = "";                                     // Single Device A1 S8
// static const char *const DRYERA1ID9 PROGMEM = "";                                     // Single Device A1 S9
// static const char *const DRYERA1ID10 PROGMEM = "";                                    // Single Device A1 S10
// static const char *const DRYERA1ID11 PROGMEM = "";                                    // Single Device A1 S11
// static const char *const DRYERA1ID12 PROGMEM = "";                                    // Single Device A1 S12
// static const char *const DRYERA1ID13 PROGMEM = "";                                    // Single Device A1 S13
// static const char *const DRYERA1ID14 PROGMEM = "";                                    // Single Device A1 S14
// static const char *const DRYERA1ID15 PROGMEM = "";                                    // Single Device A1 S15
// static const char *const DRYERA1ID16 PROGMEM = "";                                    // Single Device A1 S16
// static const char *const DRYERA1ID17 PROGMEM = "";                                    // Single Device A1 S17
// // // DRYER A2 UUID 1 - 17
// static const char *const DRYERA2ID1 PROGMEM = "";  // Single Device A2 S1
// static const char *const DRYERA2ID2 PROGMEM = "";  // Single Device A2 S2
// static const char *const DRYERA2ID3 PROGMEM = "";  // Single Device A2 S3
// static const char *const DRYERA2ID4 PROGMEM = "";  // Single Device A2 S4
// static const char *const DRYERA2ID5 PROGMEM = "";  // Single Device A2 S5
// static const char *const DRYERA2ID6 PROGMEM = "";  // Single Device A2 S6
// static const char *const DRYERA2ID7 PROGMEM = "";  // Single Device A2 S7
// static const char *const DRYERA2ID8 PROGMEM = "";  // Single Device A2 S8
// static const char *const DRYERA2ID9 PROGMEM = "";  // Single Device A2 S9
// static const char *const DRYERA2ID10 PROGMEM = ""; // Single Device A2 S10
// static const char *const DRYERA2ID11 PROGMEM = ""; // Single Device A2 S11
// static const char *const DRYERA2ID12 PROGMEM = ""; // Single Device A2 S12
// static const char *const DRYERA2ID13 PROGMEM = ""; // Single Device A2 S13
// static const char *const DRYERA2ID14 PROGMEM = ""; // Single Device A2 S14
// static const char *const DRYERA2ID15 PROGMEM = ""; // Single Device A2 S15
// static const char *const DRYERA2ID16 PROGMEM = ""; // Single Device A2 S16
// static const char *const DRYERA2ID17 PROGMEM = ""; // Single Device A2 S17
// // // DRYER A3 UUID 1 - 17
// // static const char *const DRYERA3ID1 PROGMEM = "";  // Single Device A3 S1
// // static const char *const DRYERA3ID2 PROGMEM = "";  // Single Device A3 S2
// // static const char *const DRYERA3ID3 PROGMEM = "";  // Single Device A3 S3
// // static const char *const DRYERA3ID4 PROGMEM = "";  // Single Device A3 S4
// // static const char *const DRYERA3ID5 PROGMEM = "";  // Single Device A3 S5
// // static const char *const DRYERA3ID6 PROGMEM = "";  // Single Device A3 S6
// // static const char *const DRYERA3ID7 PROGMEM = "";  // Single Device A3 S7
// // static const char *const DRYERA3ID8 PROGMEM = "";  // Single Device A3 S8
// // static const char *const DRYERA3ID9 PROGMEM = "";  // Single Device A3 S9
// // static const char *const DRYERA3ID10 PROGMEM = ""; // Single Device A3 S10
// // static const char *const DRYERA3ID11 PROGMEM = ""; // Single Device A3 S11
// // static const char *const DRYERA3ID12 PROGMEM = ""; // Single Device A3 S12
// // static const char *const DRYERA3ID13 PROGMEM = ""; // Single Device A3 S13
// // static const char *const DRYERA3ID14 PROGMEM = ""; // Single Device A3 S14
// // static const char *const DRYERA3ID15 PROGMEM = ""; // Single Device A3 S15
// // static const char *const DRYERA3ID16 PROGMEM = ""; // Single Device A3 S16
// // static const char *const DRYERA3ID17 PROGMEM = ""; // Single Device A3 S17
// // // DRYER A4 UUID 1 - 17
// // static const char *const DRYERA4ID1 PROGMEM = "";  // Single Device A4 S1
// // static const char *const DRYERA4ID2 PROGMEM = "";  // Single Device A4 S2
// // static const char *const DRYERA4ID3 PROGMEM = "";  // Single Device A4 S3
// // static const char *const DRYERA4ID4 PROGMEM = "";  // Single Device A4 S4
// // static const char *const DRYERA4ID5 PROGMEM = "";  // Single Device A4 S5
// // static const char *const DRYERA4ID6 PROGMEM = "";  // Single Device A4 S6
// // static const char *const DRYERA4ID7 PROGMEM = "";  // Single Device A4 S7
// // static const char *const DRYERA4ID8 PROGMEM = "";  // Single Device A4 S8
// // static const char *const DRYERA4ID9 PROGMEM = "";  // Single Device A4 S9
// // static const char *const DRYERA4ID10 PROGMEM = ""; // Single Device A4 S10
// // static const char *const DRYERA4ID11 PROGMEM = ""; // Single Device A4 S11
// // static const char *const DRYERA4ID12 PROGMEM = ""; // Single Device A4 S12
// // static const char *const DRYERA4ID13 PROGMEM = ""; // Single Device A4 S13
// // static const char *const DRYERA4ID14 PROGMEM = ""; // Single Device A4 S14
// // static const char *const DRYERA4ID15 PROGMEM = ""; // Single Device A4 S15
// // static const char *const DRYERA4ID16 PROGMEM = ""; // Single Device A4 S16
// // static const char *const DRYERA4ID17 PROGMEM = ""; // Single Device A4 S17

// // // DRYER A5 UUID 1 - 17
// static const char *const DRYERA5ID1 PROGMEM = "160574b8-f610-4acb-96cb-aeb32a869d05";  // Single Device A5 S1
// static const char *const DRYERA5ID2 PROGMEM = "f58443b0-e5fa-418a-89cc-fb45fb423e55";  // Single Device A5 S2
// static const char *const DRYERA5ID3 PROGMEM = "10c8c8ae-7857-4c89-a223-d0b0704a718c";  // Single Device A5 S3
// static const char *const DRYERA5ID4 PROGMEM = "86a09ccb-00eb-474b-949c-6d1932355f32";  // Single Device A5 S4
// static const char *const DRYERA5ID5 PROGMEM = "0f2b0d33-3cce-42d5-8b55-da93b8f9bccb";  // Single Device A5 S5
// static const char *const DRYERA5ID6 PROGMEM = "e00fa00c-a2d8-4ce0-a573-1619701d66c4";  // Single Device A5 S6
// static const char *const DRYERA5ID7 PROGMEM = "6e54a473-d6ae-4f2a-a0b0-bba46ce55fc4";  // Single Device A5 S7
// static const char *const DRYERA5ID8 PROGMEM = "b5661925-eeb6-455d-8497-5aaa112eb32b";  // Single Device A5 S8
// static const char *const DRYERA5ID9 PROGMEM = "d32569a7-70e2-4713-a2b2-56d287eb6ef1";  // Single Device A5 S9
// static const char *const DRYERA5ID10 PROGMEM = "9fbb3768-2efa-4b94-8328-716658e04275"; // Single Device A5 S10
// static const char *const DRYERA5ID11 PROGMEM = "de3ea239-a465-4286-bab2-f67468ffa013"; // Single Device A5 S11
// static const char *const DRYERA5ID12 PROGMEM = "b998220f-1878-4ec3-87c0-a43fe0aa5d26"; // Single Device A5 S12
// static const char *const DRYERA5ID13 PROGMEM = "d200f967-9eed-45c0-9f0d-7d39ac4796d8"; // Single Device A5 S13
// static const char *const DRYERA5ID14 PROGMEM = "c5a1cfdc-7119-409e-9e98-fea679d195fc"; // Single Device A5 S14
// static const char *const DRYERA5ID15 PROGMEM = "86f3d686-57f2-4d5e-84dd-9974b7391384"; // Single Device A5 S15
// static const char *const DRYERA5ID16 PROGMEM = "396e34b3-be2f-45f5-8706-ceb99b094449"; // Single Device A5 S16
// static const char *const DRYERA5ID17 PROGMEM = "aef53e6b-9bc5-439a-8279-a25d47519e4f"; // Single Device A5 S17

// // // DRYER A6 UUID 1 - 17
// // static const char *const DRYERA6ID3 PROGMEM = ""; // Single Device A6 S3
// // static const char *const DRYERA6ID1 PROGMEM = ""; // Single Device A6 S1
// // static const char *const DRYERA6ID2 PROGMEM = ""; // Single Device A6 S2
// // static const char *const DRYERA6ID3 PROGMEM = ""; // Single Device A6 S3
// // static const char *const DRYERA6ID1 PROGMEM = ""; // Single Device A6 S1
// // static const char *const DRYERA6ID2 PROGMEM = ""; // Single Device A6 S2
// // static const char *const DRYERA6ID3 PROGMEM = ""; // Single Device A6 S3
// // static const char *const DRYERA6ID1 PROGMEM = ""; // Single Device A6 S1
// // static const char *const DRYERA6ID2 PROGMEM = ""; // Single Device A6 S2
// // static const char *const DRYERA6ID3 PROGMEM = ""; // Single Device A6 S3
// // static const char *const DRYERA6ID1 PROGMEM = ""; // Single Device A6 S1
// // static const char *const DRYERA6ID2 PROGMEM = ""; // Single Device A6 S2
// // static const char *const DRYERA6ID3 PROGMEM = ""; // Single Device A6 S3

// const char *const dryerA1[] PROGMEM = {DRYERA1ID1, DRYERA1ID2, DRYERA1ID3, DRYERA1ID4, DRYERA1ID5, DRYERA1ID6, DRYERA1ID7, DRYERA1ID8, DRYERA1ID9, DRYERA1ID10, DRYERA1ID11, DRYERA1ID12, DRYERA1ID13, DRYERA1ID14, DRYERA1ID15, DRYERA1ID16, DRYERA1ID17};
// const char *const dryerA2[] PROGMEM = {DRYERA2ID1, DRYERA2ID2, DRYERA2ID3, DRYERA2ID4, DRYERA2ID5, DRYERA2ID6, DRYERA2ID7, DRYERA2ID8, DRYERA2ID9, DRYERA2ID10, DRYERA2ID11, DRYERA2ID12, DRYERA2ID13, DRYERA2ID14, DRYERA2ID15, DRYERA2ID16, DRYERA2ID17};
// const char *const dryerA3[] PROGMEM = {DRYERA5ID1, DRYERA5ID2, DRYERA5ID3, DRYERA5ID4, DRYERA5ID5, DRYERA5ID6, DRYERA5ID7, DRYERA5ID8, DRYERA5ID9, DRYERA5ID10, DRYERA5ID11, DRYERA5ID12, DRYERA5ID13, DRYERA5ID14, DRYERA5ID15, DRYERA5ID16, DRYERA5ID17};
// const char *const dryerA4[] PROGMEM = {DRYERA5ID1, DRYERA5ID2, DRYERA5ID3, DRYERA5ID4, DRYERA5ID5, DRYERA5ID6, DRYERA5ID7, DRYERA5ID8, DRYERA5ID9, DRYERA5ID10, DRYERA5ID11, DRYERA5ID12, DRYERA5ID13, DRYERA5ID14, DRYERA5ID15, DRYERA5ID16, DRYERA5ID17};
// const char *const dryerA5[] PROGMEM = {DRYERA5ID1, DRYERA5ID2, DRYERA5ID3, DRYERA5ID4, DRYERA5ID5, DRYERA5ID6, DRYERA5ID7, DRYERA5ID8, DRYERA5ID9, DRYERA5ID10, DRYERA5ID11, DRYERA5ID12, DRYERA5ID13, DRYERA5ID14, DRYERA5ID15, DRYERA5ID16, DRYERA5ID17};
// const char *const dryerA6[] PROGMEM = {DRYERA5ID1, DRYERA5ID2, DRYERA5ID3, DRYERA5ID4, DRYERA5ID5, DRYERA5ID6, DRYERA5ID7, DRYERA5ID8, DRYERA5ID9, DRYERA5ID10, DRYERA5ID11, DRYERA5ID12, DRYERA5ID13, DRYERA5ID14, DRYERA5ID15, DRYERA5ID16, DRYERA5ID17};
// const char *const dryerA7[] PROGMEM = {DRYERA5ID1, DRYERA5ID2, DRYERA5ID3, DRYERA5ID4, DRYERA5ID5, DRYERA5ID6, DRYERA5ID7, DRYERA5ID8, DRYERA5ID9, DRYERA5ID10, DRYERA5ID11, DRYERA5ID12, DRYERA5ID13, DRYERA5ID14, DRYERA5ID15, DRYERA5ID16, DRYERA5ID17};
// const char *const dryerA8[] PROGMEM = {DRYERA5ID1, DRYERA5ID2, DRYERA5ID3, DRYERA5ID4, DRYERA5ID5, DRYERA5ID6, DRYERA5ID7, DRYERA5ID8, DRYERA5ID9, DRYERA5ID10, DRYERA5ID11, DRYERA5ID12, DRYERA5ID13, DRYERA5ID14, DRYERA5ID15, DRYERA5ID16, DRYERA5ID17};
// const char *const dryerA9[] PROGMEM = {DRYERA5ID1, DRYERA5ID2, DRYERA5ID3, DRYERA5ID4, DRYERA5ID5, DRYERA5ID6, DRYERA5ID7, DRYERA5ID8, DRYERA5ID9, DRYERA5ID10, DRYERA5ID11, DRYERA5ID12, DRYERA5ID13, DRYERA5ID14, DRYERA5ID15, DRYERA5ID16, DRYERA5ID17};
// const char *const dryerA10[] PROGMEM = {DRYERA5ID1, DRYERA5ID2, DRYERA5ID3, DRYERA5ID4, DRYERA5ID5, DRYERA5ID6, DRYERA5ID7, DRYERA5ID8, DRYERA5ID9, DRYERA5ID10, DRYERA5ID11, DRYERA5ID12, DRYERA5ID13, DRYERA5ID14, DRYERA5ID15, DRYERA5ID16, DRYERA5ID17};
// const char *const dryerA11[] PROGMEM = {DRYERA5ID1, DRYERA5ID2, DRYERA5ID3, DRYERA5ID4, DRYERA5ID5, DRYERA5ID6, DRYERA5ID7, DRYERA5ID8, DRYERA5ID9, DRYERA5ID10, DRYERA5ID11, DRYERA5ID12, DRYERA5ID13, DRYERA5ID14, DRYERA5ID15, DRYERA5ID16, DRYERA5ID17};
// const char *const dryerA12[] PROGMEM = {DRYERA5ID1, DRYERA5ID2, DRYERA5ID3, DRYERA5ID4, DRYERA5ID5, DRYERA5ID6, DRYERA5ID7, DRYERA5ID8, DRYERA5ID9, DRYERA5ID10, DRYERA5ID11, DRYERA5ID12, DRYERA5ID13, DRYERA5ID14, DRYERA5ID15, DRYERA5ID16, DRYERA5ID17};
// const char *const dryerA13[] PROGMEM = {DRYERA5ID1, DRYERA5ID2, DRYERA5ID3, DRYERA5ID4, DRYERA5ID5, DRYERA5ID6, DRYERA5ID7, DRYERA5ID8, DRYERA5ID9, DRYERA5ID10, DRYERA5ID11, DRYERA5ID12, DRYERA5ID13, DRYERA5ID14, DRYERA5ID15, DRYERA5ID16, DRYERA5ID17};
// const char *const dryerA14[] PROGMEM = {DRYERA5ID1, DRYERA5ID2, DRYERA5ID3, DRYERA5ID4, DRYERA5ID5, DRYERA5ID6, DRYERA5ID7, DRYERA5ID8, DRYERA5ID9, DRYERA5ID10, DRYERA5ID11, DRYERA5ID12, DRYERA5ID13, DRYERA5ID14, DRYERA5ID15, DRYERA5ID16, DRYERA5ID17};
